from django.shortcuts import render
import requests
from django.core.files.base import ContentFile
import base64
import six
import uuid
import io
from io import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import Image
from django.core.files.base import ContentFile
from io import BytesIO
from PIL import Image
import json

def addmaincategory(request):
    token = request.session.get("token")
    headers = {
        'Authorization': 'token '+token,
    }
    
    if request.method == 'POST' and request.FILES['category_thumbnail']:
        category_name = request.POST.get('category_name')
        category_description = request.POST.get('category_description')
        category_thumbnail =  request.FILES['category_thumbnail']
    
        with category_thumbnail as file:
            encoded_string = base64.b64encode(file.read())

        # imgdata = base64.b64decode(encoded_string)
        # filename = f'{category_name}.png'  # I assume you have a way of picking unique filenames
        # with open(filename, 'wb') as f:
        #     f.write(imgdata)

        data = {
            'category_name': category_name,
            'category_description': category_description,
            'category_thumbnail': encoded_string
        }

        addmaincategory_response = requests.post('https://api-ecommerce.datavivservers.in/mobile_api/AdminProductMainCategory/', headers=headers, data=data)
        print(addmaincategory_response.json())
    return render(request, 'inventory/add_main_category.html')


def addsubcategory(request):
    token = request.session.get("token")
    headers = {
        'Authorization': 'token '+token,
    }

    maincategory_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/AdminProductMainCategory/', headers=headers).json()
    context = {
        'main_categories': maincategory_response
    }

    if request.method == 'POST' and request.FILES['category_thumbnail']:
        category_name = request.POST.get('category_name')
        category_description = request.POST.get('category_description')
        category_thumbnail =  request.FILES['category_thumbnail']
        main_category = request.POST.get('main_category')
    
        with category_thumbnail as file:
            encoded_string = base64.b64encode(file.read())

        data = {
            'category_name': category_name,
            'category_description': category_description,
            'category_thumbnail': encoded_string,
            'main_category': 'main_category'
        }

        addmaincategory_response = requests.post('https://api-ecommerce.datavivservers.in/mobile_api/AdminProductSubCategory/', headers=headers, data=data)
    return render(request, 'inventory/add_sub_category.html', context)

def addproducts(request):
    token = request.session.get("token")
    headers = {
        'Authorization': 'token '+token,
    }

    maincategory_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/AdminProductMainCategory/', headers=headers).json()
    subcategory_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/AdminProductSubCategory/', headers=headers).json()
    context = {
        'main_categories': maincategory_response,
        'sub_categories': subcategory_response
    }
    
    if request.method == 'POST' and request.FILES:
        product_name = request.POST.get('product_name')
        product_description = request.POST.get('product_description')
        product_image = request.FILES['product_image']
        product_image1 = request.FILES['product_image1']
        product_image2 = request.FILES['product_image2']
        product_image3 = request.FILES['product_image3']
        product_price = request.POST.get('product_price')
        product_sgst = request.POST.get('product_sgst')
        product_cgst = request.POST.get('product_cgst')
        subscriptionProduct = request.POST.get('subscriptionProduct')
        product_main_category = request.POST.get('product_main_category')
        product_sub_category = request.POST.get('product_sub_category')
        productPackaging = request.POST.get('productPackaging')


        with product_image as file:
            encoded_product_image = base64.b64encode(file.read())


        if product_image1 == None:
            data = {
                'product_name': product_name,
                'product_description': product_description,
                'product_image': encoded_product_image,
                'product_price': product_price,
                'sgst': product_sgst,
                'cgst': product_cgst,
                'subscription': subscriptionProduct,
                'product_main_category': product_main_category,
                'product_sub_category': product_sub_category,
                'product_choice': productPackaging
            }
        elif product_image2 == None:
            with product_image1 as file:
                encoded_product_image1 = base64.b64encode(file.read())
            data = {
                'product_name': product_name,
                'product_description': product_description,
                'product_image': encoded_product_image,
                'product_image1': encoded_product_image1,
                'product_price': product_price,
                'sgst': product_sgst,
                'cgst': product_cgst,
                'subscription': subscriptionProduct,
                'product_main_category': product_main_category,
                'product_sub_category': product_sub_category,
                'product_choice': productPackaging
            }
        elif product_image3 == None:
            with product_image1 as file:
                encoded_product_image1 = base64.b64encode(file.read())

            with product_image2 as file:
                encoded_product_image2 = base64.b64encode(file.read())

            data = {
                'product_name': product_name,
                'product_description': product_description,
                'product_image': encoded_product_image,
                'product_image1': encoded_product_image1,
                'product_image2': encoded_product_image2,
                'product_price': product_price,
                'sgst': product_sgst,
                'cgst': product_cgst,
                'subscription': subscriptionProduct,
                'product_main_category': product_main_category,
                'product_sub_category': product_sub_category,
                'product_choice': productPackaging
            } 
        else:
            with product_image1 as file:
                encoded_product_image1 = base64.b64encode(file.read())

            with product_image2 as file:
                encoded_product_image2 = base64.b64encode(file.read())

            with product_image3 as file:
                encoded_product_image3 = base64.b64encode(file.read())

            data = {
                'product_name': product_name,
                'product_description': product_description,
                'product_image': encoded_product_image,
                'product_image1': encoded_product_image1,
                'product_image2': encoded_product_image2,
                'product_image3': encoded_product_image3,
                'product_price': product_price,
                'sgst': product_sgst,
                'cgst': product_cgst,
                'subscription': subscriptionProduct,
                'product_main_category': product_main_category,
                'product_sub_category': product_sub_category,
                'product_choice': productPackaging
            }

        addproduct_response = requests.post('https://api-ecommerce.datavivservers.in/mobile_api/AdminProducts/', headers=headers, data=data)
        print(addproduct_response.json())

    return render(request, 'inventory/add_product.html', context)

def allproducts(request):
    token = request.session.get("token")
    headers = {
        'Authorization': 'token '+token,
    }

    allproducts_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/AdminProducts/', headers=headers).json()
    context = {
        'allproducts': allproducts_response
    }

    print(allproducts_response)

    return render(request, 'inventory/allproducts.html', context)