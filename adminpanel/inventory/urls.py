from django.urls import path, include
from inventory import views


urlpatterns = [
    path('add_main_category/', views.addmaincategory, name='addmaincategory'),
    path('add_sub_category/', views.addsubcategory, name='addsubcategory'),
    path('add_products/', views.addproducts, name='addproducts'),
    path('all-products/', views.allproducts, name='allproducts'),
]