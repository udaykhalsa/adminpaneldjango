from django.shortcuts import render, redirect
import requests



def login(request):
    if request.session.get('token', None) != None:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            data = {'username': username, 'password': password}
            loginresponse = requests.post('https://api-ecommerce.datavivservers.in/mobile_api/AdminLogin/', data=data)
            logindata = loginresponse.json()
            if loginresponse.status_code == 200:
                request.session['token'] = logindata['token']
                return redirect('home')
            else:
                print('wrong')
        return render(request, 'users/login.html')


def logout(request):
    request.session.clear()
    return redirect('login')