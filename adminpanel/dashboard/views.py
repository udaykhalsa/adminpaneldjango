from django.shortcuts import render, redirect
import requests
from django.http import HttpResponseRedirect

def home(request):
    if request.session.get('token', None) != None:
        token = request.session.get("token")
        return render(request, 'dashboard/dashboard.html')  
    else:
        return redirect('login')

def topstores(request):
    top_stores_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/OverAllStoresMostOrders/')
    top_stores_data = top_stores_response.json()
    context = {
        'stores_data': top_stores_data['data']
    }
    return render(request, 'dashboard/top_stores.html', context)

def topstores_weekly(request):
    top_stores_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/StoresMostOrders/')
    top_stores_data = top_stores_response.json()
    context = {
        'stores_data': top_stores_data['data']
    }
    return render(request, 'dashboard/top_stores_weekly.html', context)

def stores_assigned_offers(request):
    return render(request, 'dashboard/offer_assigned_to_stores.html')