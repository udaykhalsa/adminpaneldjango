from django.urls import path, include
from dashboard.views import home, topstores, topstores_weekly, stores_assigned_offers


urlpatterns = [
    path('', home, name="home"),
    path('stores_assigned_offers/', stores_assigned_offers, name="stores_assigned_offers"),
    path('top-stores/', topstores, name="topstores"),
    path('top-stores-weekly/', topstores_weekly, name="topstores_weekly"),
]