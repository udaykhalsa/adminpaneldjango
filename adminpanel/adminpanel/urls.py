from django.contrib import admin
from django.urls import path, include
from adminpanel import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('complaints.urls')),
    path('', include('dashboard.urls')),
    path('', include('deliveryboys.urls')),
    path('', include('inventory.urls')),
    path('', include('offers.urls')),
    path('', include('orders.urls')),
    path('', include('stores.urls')),
    path('', include('users.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)