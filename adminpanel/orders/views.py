from django.shortcuts import render
import requests

def ordersdashboard(request):
    return render(request, 'orders/ordersdashboard.html')


def pendingorders(request):
    pendingorders_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/CurrentPlaceOrders/')
    pendingorders_data = pendingorders_response.json()
    orders = pendingorders_data['data']
    return render(request, 'orders/pendingorders.html', {'pending_orders': orders})


def allorder(request):
    context = {}
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    all_order_url = "https://api-ecommerce.datavivservers.in/mobile_api/orderhistory/"
    order_response = requests.get(all_order_url, headers = headers)
    order_response_data = order_response.json()
    context["order"] = order_response_data["data"]
    return render (request, 'orders/allorder.html', context)