from django.urls import path, include
from orders import views

urlpatterns = [
    path('orders-dashboard/', views.ordersdashboard, name="ordersdashboard"),
    path('pending-orders/', views.pendingorders, name="pendingorders"),
    path('allorder/', views.allorder, name="allorder"),
]