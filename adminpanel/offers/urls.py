from django.urls import path, include
from .views import LoyalityOffer, loyality_offer_create, buy_get_offer_view, add_buy_get_offer, user_product_offer, loyality_offer_delete, delete_user_product_offer, delete_buy_get_offer,add_user_product_offer


urlpatterns = [
    path("loyaltyoffer/", LoyalityOffer.as_view(), name= "loyalityoffer"),
    path("loyaltyoffer/add/", loyality_offer_create, name= "add_offer"),
    path("loyaltyoffer/delete/<int:id>/", loyality_offer_delete ),
    path("buygetoffer/", buy_get_offer_view, name ="buy_get_offer" ),
    path("buygetoffer/add/",add_buy_get_offer, name= "add_buy_get_offer"),
    path("buygetoffer/delete/<int:id>/", delete_buy_get_offer),
    path("productoffer/", user_product_offer, name= "product_offer"),
    path("productoffer/add/",add_user_product_offer ),
    path("productoffer/delete/<int:id>/", delete_user_product_offer),
    
]