from django.http import response
from django.shortcuts import render, redirect
from django.views import View
import requests


# Create your views here.
HOST_NAME = "https://api-ecommerce.datavivservers.in/mobile_api/"

class LoyalityOffer(View):
    def get(self, request):
        token = request.session.get("token")
        url = HOST_NAME + "loyaltyoffer/"
        headers = {
            'Authorization': 'token '+token,
            }
        response = requests.request("GET", url, headers=headers).json()
        context = {"offer" : response["LoyaltyOffer"]}
        return render (request, "offers/offerlist.html", context)

def loyality_offer_delete(request, id):

    url = HOST_NAME + "loyaltyoffer/" + str(id)
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    response = requests.delete(url, headers=headers)
    return redirect ("loyalityoffer")



def loyality_offer_create(request):
    context = {}
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    url_store = HOST_NAME + "AdminStores/"
    url_vallet = HOST_NAME + "userlist/"
    url_post = HOST_NAME + "loyaltyoffer/"

    allstores_response = requests.get(url_store, headers=headers)
    allstores_data = allstores_response.json()
    context['store_data'] = allstores_data

    userlist_response = requests.get(url_vallet, headers= headers)
    userlist_data = userlist_response.json()
    context["wallet"] = userlist_data["data"]


    if request.method == "POST":
        
        name = request.POST.get("name")
        phone_number = request.POST.get('phone_number')
        promocode = request.POST.get('promocode')
        wallet = request.POST.get('wallet')
        store = request.POST.get('store')
        data = {
            "name" : name,
            "phone_number" : phone_number,
            "promocode" : promocode,
            "wallet" : wallet,
            "store":  store,
        }
        
        response = requests.post(url_post, headers=headers, data=data)
        return redirect('loyalityoffer')
    return render (request , "offers/offer_add.html", context)
        
# def update_loyalty_offer(request, id):
#     context = {}
#     token = request.session.get("token")
#     headers = {'Authorization': 'token '+token}
#     url_store = HOST_NAME + "AdminStores/"
#     url_vallet = HOST_NAME + "userlist/"
#     url_put = HOST_NAME + "loyaltyoffer/" +str(id)
#     url_get = HOST_NAME + "loyaltyoffer/" + f'?id={id}'


#     allstores_response = requests.get(url_store, headers=headers)
#     allstores_data = allstores_response.json()
#     context['store_data'] = allstores_data

#     userlist_response = requests.get(url_vallet, headers= headers)
#     userlist_data = userlist_response.json()
#     context["wallet"] = userlist_data["data"]

#     offer_detail = requests.get(url_get, headers= headers).json()
#     context["offer_data"] = offer_detail["LoyaltyOffer"]

#     return render (request , "offers/offer_add.html", context)



def buy_get_offer_view(request):
    url = "https://api-ecommerce.datavivservers.in/mobile_api/buygetoffer/"
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    response = requests.get(url, headers = headers).json()
    offer = response['BuyGetOffer']
    context = {}
    if offer:
        context = {
            "offer" : offer
        }
    return render(request, 'offers/buygetoffer.html', context)

def delete_buy_get_offer(request,id):
    url = HOST_NAME + "buygetoffer/" + id
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    response = requests.delete(url, headers=headers)
    return redirect("buy_get_offer")


def add_buy_get_offer(request):
    context = {}
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}

    url_product = HOST_NAME + "ProductDetailForCustomer/"
    url_store = HOST_NAME + "AdminStores/"

    product_response = requests.get(url_product, headers=headers).json()
    context["product"] = product_response["data"]

    allstores_response = requests.get(url_store, headers=headers)
    allstores_data = allstores_response.json()
    context['store_data'] = allstores_data
    
    if request.method == "POST":
        store = request.POST.get("store", None)
        product_id = request.POST.get("product_id", None)
        Get_quantity = request.POST.get("Get_quantity", None)
        Buy_quantity = request.POST.get("Buy_quantity", None)
        data = {
            "store" : store,
            "Product" : product_id,
            "Get_quantity": Get_quantity,
            "Buy_quantity" : Buy_quantity
        }
        url = "https://api-ecommerce.datavivservers.in/mobile_api/buygetoffer/"
        token = request.session.get("token")
        headers = {'Authorization': 'token '+token}
        response = requests.post(url, headers = headers, data =data )
        return redirect ("buy_get_offer")
    return render(request, 'offers/addbuygetoffer.html', context)

        
def user_product_offer(request):
    context = {}
    url = "https://api-ecommerce.datavivservers.in/mobile_api/Userproductoffer/"
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    offer_response = requests.get(url, headers= headers).json()
    if offer_response:
        context["offer"] = offer_response
    return render (request, "offers/productoffer.html", context)

def add_user_product_offer(request):
    context = {}
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}

    url_store = HOST_NAME + "AdminStores/"
    allstores_response = requests.get(url_store, headers=headers)
    allstores_data = allstores_response.json()
    context['store_data'] = allstores_data

    url_product_category = HOST_NAME + "ProductCategoryDetails/"
    product_category_response = requests.get(url_product_category, headers=headers).json()
    all_product_category = product_category_response["data"]
    context["product_catgory"] = all_product_category

    url_product = HOST_NAME + "ProductDetailForCustomer/"
    product_response = requests.get(url_product, headers=headers).json()
    context["product"] = product_response["data"]
    if request.method == "POST":
        user_product_quantity = request.POST.get('user_product_quantity')
        user_buy_product = request.POST.get('user_buy_product')
        user_product_category = request.POST.get('user_product_category')
        store = request.POST.get('store')
        data = {
            "user_buy_product" : user_buy_product,
            "user_product_quantity" :user_product_quantity,
            "store" : store,
            "user_product_category" : user_product_category
        }
        url = HOST_NAME + "Userproductoffer/"
        response = requests.post(url, headers = headers, data = data)
        print(data)
        print(response)
        return redirect ("product_offer")
    return render (request, "offers/addproductoffer.html", context)


def delete_user_product_offer(request, id):
    url = HOST_NAME + "Userproductoffer/" + id
    token = request.session.get("token")
    headers = {'Authorization': 'token '+token}
    response = requests.delete(url, headers=headers)
    return redirect("product_offer")