from django.urls import path, include
from deliveryboys.views import deliveryboylist, adddeliveryboy

urlpatterns = [
    path('deliveryboy_list/', deliveryboylist, name='deliveryboylist'),
    path('adddeliveryboy/', adddeliveryboy, name='adddeliveryboy'),
]