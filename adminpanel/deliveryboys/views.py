from django.shortcuts import render, redirect
import requests
from django.http import HttpResponseRedirect

def deliveryboylist(request):
    deliveryboyresponse = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/GetAllDeliveryBoyDetails')
    deliveryboydata = deliveryboyresponse.json()
    if deliveryboyresponse.status_code == 200:
        context = {
            'deliveryboys': deliveryboydata['data']
        }
        print(context)
    return render(request, 'deliveryboys/deliveryboy_list.html', context)

def adddeliveryboy(request):
    if request.method == "POST":
        full_name = request.POST.get('full_name')
        mob_no = request.POST.get('mobile_number')
        password = request.POST.get('password')

        data = {
            'full_name': full_name,
            'mob_no': mob_no,
            'password': password,
        }

        token = request.session.get("token")
        headers = {
            'Authorization': 'token '+token
        }


        adddeliveryboyresponse = requests.post('https://api-ecommerce.datavivservers.in/mobile_api/CreateDeliveryBoyRegister/', headers=headers, data=data)
        if adddeliveryboyresponse.status_code == 200:
            return redirect('deliveryboylist')
        else:
            return redirect('home')
    
    return render (request , 'deliveryboys/addDileveryBoy.html')