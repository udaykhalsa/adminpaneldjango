from django.apps import AppConfig


class DeliveryboysConfig(AppConfig):
    name = 'deliveryboys'
