from django.shortcuts import render
import requests
import base64

def allstores(request):
    allstores_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/AllVendorStoreDetails/')
    allstores_data = allstores_response.json()
    print(allstores_data['data'])
    context = {
        'store_data': allstores_data['data']
    }

    return render(request, 'stores/allstores.html', context)

def addstore(request):
    token = request.session.get("token")
    headers = {
        'Authorization': 'token '+token,
    }

    if request.method == 'POST' and request.FILES:
        store_name = request.POST.get('store_name')
        store_display_name = request.POST.get('store_display_name')
        store_address = request.POST.get('store_address')
        store_city = request.POST.get('store_city')
        store_contact_person = request.POST.get('store_contact_person')
        store_contact_number = request.POST.get('store_contact_number')
        store_latitude = request.POST.get('store_latitude')
        store_longitude = request.POST.get('store_longitude')
        store_gstin_number = request.POST.get('store_gstin_number')
        store_pan_number = request.POST.get('store_pan_number')
        store_gstin_document = request.FILES['store_gstin_document']
        store_pan_document = request.FILES['store_pan_document']

        with store_pan_document as file:
            store_pan_document_encoded = base64.b64encode(file.read())

        with store_gstin_document as file:
            store_gstin_document_encoded = base64.b64encode(file.read())

        data = {
            'store_name': store_name,
            'store_display_name': store_display_name,
            'store_address': store_address,
            'store_city': store_city,
            'store_contact_person': store_contact_person,
            'store_contact_number': store_contact_number,
            'store_latitude': store_latitude,
            'store_longitude': store_longitude,
            'store_gstin_number': store_gstin_number,
            'store_pan_number': store_pan_number,
            'store_gstin_document': store_gstin_document_encoded,
            'store_pan_document': store_pan_document_encoded,

        }

        addstore_response = requests.post('https://api-ecommerce.datavivservers.in/mobile_api/AdminStores/', headers=headers, data=data)

    return render(request, 'stores/add_stores.html')
        

def search_allstores(request):
    if request.method == "POST":
        search_keyword = request.POST.get('search_keyword')
        print(search_keyword)

        search_list = []
        allstores_response = requests.get('https://api-ecommerce.datavivservers.in/mobile_api/AllVendorStoreDetails/')
        allstores_data = allstores_response.json()
        allstores = allstores_data['data']
        search_list = [x for x in allstores if search_keyword.upper() in x['store_name']]

        if search_list:
            return render(request, 'stores/store_search_output.html', {'search_list': search_list})
        else:
            search_list = [x for x in allstores if search_keyword.upper() in x['city']]
            if search_list:
                return render(request, 'stores/store_search_output.html', {'search_list': search_list})
            else:
                search_list = [x for x in allstores if search_keyword.upper() in x['address']]
                return render(request, 'stores/store_search_output.html', {'search_list': search_list})

#https://api-ecommerce.datavivservers.in/mobile_api/AdminProductMainCategory/
#https://api-ecommerce.datavivservers.in/mobile_api/AdminProductMainCategory/<str:pk>
#
#https://api-ecommerce.datavivservers.in/mobile_api/AdminProductSubCategory/
#https://api-ecommerce.datavivservers.in/mobile_api/AdminProductSubCategory/<str:pk>
#
#https://api-ecommerce.datavivservers.in/mobile_api/AdminProducts/<str:pk>
#
#https://api-ecommerce.datavivservers.in/mobile_api/AdminStores/
#https://api-ecommerce.datavivservers.in/mobile_api/AdminStores/<str:pk>
