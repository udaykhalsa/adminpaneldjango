from django.urls import path, include
from stores import views


urlpatterns = [
    path('allstores/', views.allstores, name='allstores'),
    path('store-search/', views.search_allstores, name='search_allstores'),
    path('add-store/', views.addstore, name='addstore'),
]